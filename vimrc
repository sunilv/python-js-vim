set nocompatible
filetype off
set encoding=utf-8
set clipboard=unnamed
syntax on
set nu
set t_Co=256
set cursorline

let python_highlight_all=1
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 3

"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

au BufNewFile,BufRead *.py
    \ set tabstop=4
    \| set softtabstop=4
    \| set shiftwidth=4
    \| set textwidth=88
    \| set expandtab
    \| set autoindent
    \| set fileformat=unix

au BufNewFile,BufRead *.js,*.html,*.css
    \ set tabstop=2
    \| set softtabstop=2
    \| set shiftwidth=2

" remove unneccesary whitespaces
highlight BadWhitespace ctermbg=red guibg=darkred
au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

call plug#begin('~/.vim/plugged')
" Plugins 
Plug 'psf/black', { 'tag': '19.10b0','for':'python' }
Plug 'vim-syntastic/syntastic'
Plug 'nvie/vim-flake8', {'for':'python'}
Plug 'kien/ctrlp.vim'
Plug 'jmcantrell/vim-virtualenv' , {'for':'python'}
Plug 'fisadev/vim-isort'  , {'for':'python'}
Plug 'pangloss/vim-javascript',{ 'for': ['javascript', 'typescript']} 
Plug 'leafgarland/typescript-vim',{ 'for': ['javascript', 'typescript']} 
Plug 'maxmellon/vim-jsx-pretty',{ 'for': ['javascript', 'typescript']} 
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
Plug 'terryma/vim-multiple-cursors'
Plug 'wakatime/vim-wakatime'
Plug 'sonph/onehalf', {'rtp': 'vim/'}
Plug 'Valloric/YouCompleteMe'
" end of plugin
call plug#end()
"set statusline+=%{exists('g:loaded_syntastic_plugin')?SyntasticStatuslineFlag():''}
let g:ycm_autoclose_preview_window_after_completion=1
"map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

colorscheme onehalfdark
"colorscheme smyck

"let g:airline_theme='onehalfdark'

" use ripgrep is search engine
if executable('rg')
  set grepprg=rg\ --color=never
  let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'
  let g:ctrlp_use_caching = 0
endif

"Format .py files on save
" Run Black on save.
autocmd BufWritePre *.py execute ':Black'
"isort map
let g:vim_isort_map = '<C-i>'
" FORMATTERS
let g:prettier#autoformat = 0
autocmd BufWritePre *.js,*.jsx,*.mjs,*.ts,*.tsx,*.css,*.less,*.scss,*.json,*.graphql,*.md,*.vue,*.yaml,*.html PrettierAsync

filetype plugin indent on    