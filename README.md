# Simple Vim config for Python and javascript (React)

## Intructions
- Install Vimplug
    ```curl -fLo ~/.var/app/io.neovim.nvim/data/nvim/site/autoload/plug.vim \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    ```
- Reload .vimrc and :PlugInstall to install plugins.


## Plugin Used
```
Plugin 'gmarik/Vundle.vim'
Plugin 'psf/black', { 'tag': '19.10b0' }
Plugin 'vim-syntastic/syntastic'
Plugin 'nvie/vim-flake8'
Plugin 'kien/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'jmcantrell/vim-virtualenv' 
Plugin 'fisadev/vim-isort'
Plugin 'pangloss/vim-javascript'
Plugin 'leafgarland/typescript-vim'
Plugin 'maxmellon/vim-jsx-pretty'
Plugin 'prettier/vim-prettier', { 'do': 'yarn install', 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }
Plugin 'terryma/vim-multiple-cursors'
Plugin 'vim-airline/vim-airline'
Plugin 'wakatime/vim-wakatime'
Plugin 'dim13/smyck.vim' 
Bundle 'sonph/onehalf', {'rtp': 'vim/'}
Bundle 'Valloric/YouCompleteMe'
```

